# SQLAlchemy - 초보자를 위한 짧은 소개 <sup>[1](#footnote_1)</sup>

![](./1_Kzy4QCGANrtaHJX-w4T-QA.webp)

강력하고 다재다능한 Python용 SQL 툴킷이자 객체 관계형 매핑(ORM) 시스템인 SQLAlchemy는 효율적이고 고성능의 데이터베이스 액세스를 위해 설계된 잘 알려진 엔터프라이즈급 지속성 패턴의 전체 제품군을 제공한다. 즉, 작고 간단한 쿼리부터 부하가 많은 시나리오의 복잡한 트랜잭션까지 모든 것을 처리할 수 있다.

<a name="footnote_1">1</a>: 이 페이지는 [sqlalchemy-a-short-introduction-for-beginners](https://levelup.gitconnected.com/introduction-to-sqlalchemy-bbf5af69a458)을 편역한 것이다.
