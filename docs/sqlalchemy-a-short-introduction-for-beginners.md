# SQLAlchemy - 초보자를 위한 짧은 소개

## SQLAlchemy 설치
먼저 pip을 사용하여 설치한다.

```bash
$ pip install SQLAlchemy
```

## 기초 개념
코드를 살펴보기 전에 몇 가지 기본 개념을 설명한다.

### 엔진(Engine)
엔진은 모든 SQLAlchemy 어플리케이션의 시작점이다. 실제 데이터베이스와 해당 DBAPI의 'home base'이며, 연결 풀과 Dialect를 통해 SQLAlchemy 어플리케이션에 전달된다.

### 세션(Session)
세션은 수명 기간 동안 로드하거나 연결한 모든 객체를 보관하는 영역이다. 세션 객체의 현재 데이터베이스 연결을 사용하여 데이터베이스에 쿼리를 전송하는 쿼리 객체를 가져올 수 있는 엔트리포인트 제공한다.

### ORM (Object-Relational Mapping)
ORM은 객체 지향 프로그래밍 언어를 사용하여 호환되지 않는 타입 시스템 간에 데이터를 변환하는 프로그래밍 기법이다. 이는 사실상 프로그래밍 언어 내에서 사용할 수 있는 '가상 객체 데이터베이스'를 생성한다. SQLAlchemy에는 관계형 데이터베이스 스키마와 작업을 처리하기 위한 ORM 계층이 포함되어 있다.

## 코드 예

### 데이터베이스에 연결
먼저 데이터베이스에 연결해 보겠다. 이 작업은 다음과 같이 생성할 수 있는 엔진을 통해 수행된다.

```python
from sqlalchemy import create_engine

engine = create_engine('sqlite:///example.db')
```

이렇게 하면 `example.db`라는 SQLite 데이터베이스와 상호 작용할 엔진이 생성된다.

## 테이블 생성
다음으로 테이블을 만들어 보자. 먼저 몇 가지 컴포넌트를 더 가져와야 한다.

```python
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(String)
```

위 코드에서는 테이블 이름과 여러 개의 컬럼이 있는 User 클래스를 정의하고 있다. 또한 관계형 데이터베이스 테이블에 매핑되는 Python 클래스의 생성과 조작을 간소화하는 SQLAlchemy의 선언적 베이스 시스템을 사용하고 있다.

실제로 데이터베이스에 테이블을 생성하려면 `Bawe` 클래스의 `create_all()` 메서드를 사용하여 엔진을 인수로 전달한다.

```python
Base.metadata.create_all(engine)
```

## 레코드 삽입
레코드를 삽입하려면 먼저 User 클래스의 인스턴스를 생성한다. 그런 다음 새 세션을 만들고 사용자를 추가한 다음 세션을 커밋한다.

```python
from sqlalchemy.orm import Session

# Create a new session
session = Session(engine)

# Create a new user
new_user = User(name='John Doe', email='john@doe.com')

# Add the user to the session
session.add(new_user)

# Commit the session
session.commit()
```

## 데이터 검색
레코드를 삽입한 후 데이터베이스에서 데이터를 쿼리할 수 있다. SQLAlchemy는 이를 위한 `Session.query()` 메서드를 제공한다.

```python
# Query the database
users = session.query(User).all()

# Print all users
for user in users:
    print(user.name, user.email)

# John Doe john@doe.com
```

위 코드에서는 데이터베이스의 모든 사용자를 쿼리한 다음 이름과 이메일을 출력한다.

## 마치며
SQLAlchemy는 Python에서 데이터베이스를 처리하기 위한 강력한 도구이다. 다양한 기능과 유연성을 갖추고 있어 모든 규모의 프로젝트에 탁월한 선택이 될 수 있다. 여기서는 SQLAlchemy로 할 수 있는 일의 표면적인 부분만 살펴봤다. 복잡한 쿼리, 관계, 트랜잭션 등과 같은 다른 많은 기능을 살펴볼 수 있다. 계속 지켜봐 주세요!
